#!/usr/bin/env python3
import keras
from keras import layers
"""
Simple toy network, 3! consecutive conv2D layers that can be fused
"""


inp = keras.Input(shape=(22, 22, 3))
x = layers.Conv2D(4, (3, 3), activation="linear",
                  padding='valid', name='L0')(inp)
x = layers.Conv2D(4, (3, 3), activation="linear",
                  padding='valid', name='L1')(x)
out = layers.Conv2D(4, (3, 3), activation="linear",
                    padding='valid', name='L2')(x)
model = keras.Model(
    inputs=[inp],
    outputs=[out],
    name=__file__[:-3]
)
model.summary()
model.save(__file__.replace('.py', '.h5'))
