from ..utils import listify
import numpy as np


def remove_node(G, n):
    # remove node with a single output

    # get output tensor
    out = n.cfg.outputs
    assert(len(out) == 1 and "Can only remove nodes with one output tensor")
    out = out[0]

    # for all successors
    for suc in G.successors(n):

        # scan down to true successor
        tsuc = suc
        while tsuc.merged_out is not None:
            tsuc = tsuc.merged_out

        # Create bypass edges from predecessors
        for pred in G.predecessors(n):

            # scan down to true predecessor
            tpred = pred
            while tpred.merged_in is not None:
                tpred = tpred.merged_in

            # Add bypass for each output of the predecessor
            for output_tensor in listify(tpred.output):
                G.add_edge(pred, suc, name=str(output_tensor.name))

                # replace n.output_tensor with pred.output
                tsuc.cfg.inputs = [
                    output_tensor if inp.ref() == out.ref() else inp
                    for inp in tsuc.cfg.inputs
                ]

    # Finally delete node from graph
    G.remove_node(n)


def merge_down(G, n):
    # set n as merged_in root for all successors
    for suc in G.successors(n):
        # find root merge node
        while suc.merged_in is not None:
            suc = suc.merged_in
        suc.merged_in = n

    # finally remove n from graph
    remove_node(G, n)


def merge_up(G, n):
    # set n as merged_out rout for all predecessors
    for pred in G.predecessors(n):
        # find root merge node
        while pred.merged_out is not None:
            pred = pred.merged_out
        pred.merged_out = n

    # finally remove n from graph
    remove_node(G, n)


def padding(G, l):
    # starting node has to be conv2D
    if l.type not in ['conv2d']:
        return False

    # Predecessor must be single Padding node
    p = [p for p in G.predecessors(l)]
    if not (len(p) == 1 and p[0].type in ['zeropadding2d']):
        return False
    pad_lyr = p[0]

    # predecessor of pad_layer (also must be only 1)
    pp = [pp for pp in G.predecessors(pad_lyr)]
    if not (len(pp) == 1):
        return False
    pp = pp[0]

    # Find true output node (if already merged)
    true_out_node = pp
    while true_out_node.merged_out is not None:
        true_out_node = true_out_node.merged_out

    # Match tensors and set new input dimensions
    for out in listify(true_out_node.output):
        if out.ref() in [i.ref() for i in listify(pad_lyr.input)]:
            l.cfg.in_height = out.shape[1]
            l.cfg.in_width = out.shape[2]

    # set padding property on conv2D layer
    l.cfg.padding_type = 'same'
    l.cfg.padding = pad_lyr.padding

    # Merge padding layer down into conv layer
    merge_down(G, pad_lyr)

    print("Fusing padding layer '%s' into '%s'" % (pad_lyr.name, l.name))
    return True


def batchnorm(G, l):

    # starting node has to be conv2D
    if l.type not in ['conv2d']:
        return False

    # Successor must be single batchnorm layer
    s = [s for s in G.successors(l)]
    if not (len(s) == 1 and s[0].type in ['batchnormalization']):
        return False
    bn = s[0]

    # verify fused batchnorm is OK
    if not bn.fused:
        return False

    # extract current weights and bias
    if l.use_bias:
        weights, bias = l.get_weights()
    else:
        weights = l.get_weights()[0]
        bias = np.zeros(weights.shape[-1], dtype='float32')

    # Extract parameters of batchnorm layer
    if bn.center and bn.scale:
        gamma, beta, mean, variance = tuple(bn.get_weights())
    elif not bn.center and bn.scale:
        gamma, mean, variance = tuple(bn.get_weights())
        beta = np.zeros(shape=gamma.shape, dtype='float32')
    elif bn.center and not bn.scale:
        beta, mean, variance = tuple(bn.get_weights())
        gamma = np.ones(shape=beta.shape, dtype='float32')
    else:
        mean, variance = tuple(bn.get_weights())
        beta = np.zeros(shape=mean.shape, dtype='float32')
        gamma = np.ones(shape=mean.shape, dtype='float32')

    gamma = gamma.reshape((1, 1, 1, gamma.shape[0]))
    variance = variance.reshape((1, 1, 1, variance.shape[0]))

    # compute scaling factor
    scaling_factor = gamma / np.sqrt(variance + bn.epsilon)

    # scale weights
    weights *= scaling_factor

    # compute bias
    bias = beta + (bias - mean) * scaling_factor

    # Update weights of conv layer
    l.cfg.use_bias = True
    l.cfg.get_weights = lambda: [weights, bias.reshape(bias.shape[-1])]

    # merge batchnorm layer up into conv2d
    merge_up(G, bn)

    print("Fusing batch norm '%s' into '%s'" % (bn.name, l.name))
    return True


def relu(G, l):
    # starting node has to be conv2D
    if l.type not in ['conv2d']:
        return False

    # Successor must be single rele layer
    s = [s for s in G.successors(l)]
    if not (len(s) == 1 and (
        s[0].type in ['relu'] or (
            s[0].type in ['activation'] and
            s[0].activation.__name__ in ['relu']
        )
    )):
        return False
    r = s[0]

    # Current activation must be pass
    if not (
        l.activation is None or
        l.activation.__name__.lower() in ['linear']
    ):
        return False

    # Can only handle standard relu, and max_value parameter
    if not hasattr(r, 'activation'):
        if r.negative_slope != 0 or r.threshold != 0:
            return False
        if r.max_value is not None:
            l.cfg.act.max_value = r.max_value

    # Update conv2d layer
    l.cfg.act.type = 'relu'

    # remove relu from graph
    merge_up(G, r)

    print("Fusing relu '%s' into '%s'" % (r.name, l.name))
    return True


def canonicalize(G):

    # selected fuse passes
    passes = [padding, batchnorm, relu]

    # Keep trying to match on the graph until there are no more matches
    # note, not exactly smart, but guaranteed to be safe
    changes = True
    while changes:

        # Loop over nodes until graph match is found
        changes = False
        for l in G.nodes():
            changes = any([passfunc(G, l) for passfunc in passes])
            if changes:
                break

