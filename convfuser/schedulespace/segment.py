from .schedule import Segment, Layer
from .layer import layer_space, layer_space_recompute
from ..costmodel import layer as layer_cost
from ..costmodel import segment as segment_cost
from ..utils import pareto, max2consec
from functools import reduce
import operator as op
import numpy as np


def segments(l, max_fused=None, min_fused=None):
    """ Helper function to generate all unique segments """
    if max_fused is None:
        max_fused = len(l)
    if min_fused is None:
        min_fused = 1
    for span in range(min_fused - 1, min(max_fused, len(l))):
        for start in range(0, len(l) - span):
            yield l[start:start + span + 1]


def segment_space(seq, args=None):

    # Get maximum number of fused segments
    max_fused = None if args is None else args.max_fused
    min_fused = None if args is None else args.min_fused

    costs = segment_space_norecompute(seq, min_fused, max_fused, args=args)
    if args.recompute:
        costs.update(segment_space_recompute(
            seq, min_fused, max_fused, args=args))
    return costs


def segment_space_norecompute(seq, min_fused, max_fused, args=None):
    # dictionary that holds per segment a set of costs+schedules
    segment2costs = {}

    for seg in segments(seq, max_fused=max_fused, min_fused=min_fused):
        print('  Exploring segment:', [n.name for n in seg])
        costs = set()

        last = seg[-1]
        for idx, sched in enumerate(layer_space(
            last, tiling_func=args.tiling_func
        )):
            # build other schedules based on last, except match Ti=Di and To=Do
            # per layer
            scheds = [
                Layer(
                    sched._sl, sched._order,
                    [sched._T[0], sched._T[1], l.cfg.vol[2], l.cfg.vol[3]]
                ) for l in seg[:-1]
            ] + [sched]

            # all layers are scheduled the same
            ss = Segment([seg] + scheds)
            ss.buffered_input = False
            ss.recompute = False

            # get costs using traditional models
            acc, buf, macs = segment_cost(ss, verbose=False)

            # any double costs will simply be overwritten
            costs.add((acc, buf, macs, ss))

            ########################################
            # Check if exploration cap is reached
            if args is not None and args.exp_cap is not None and idx >= args.exp_cap - 1:
                print('Exiting from DSE, exceeded exploration cap')
                break

        # pareto filter section costs
        arr = np.array(list(costs))
        if args.pareto:
            arr = pareto(arr)
            print("  Explored %d schedules, of which %d are pareto" %
                  (idx + 1, len(arr)))
        else:
            print("  Explored %d schedules" % (idx + 1))
        segment2costs[tuple(seg)] = arr

    return segment2costs


def segment_space_recompute(seq, min_fused, max_fused, args=None):
    """ Get pareto schedules for segment"""

    # dictionary that holds per segment a set of costs+schedules
    segment2costs = {}

    # Iterate over all segments in this sequence
    for seg in segments(seq, max_fused=max_fused, min_fused=min_fused):
        print('  Exploring segment with recomputation:', [n.name for n in seg])

        # Get cost of segment in recursive manner
        def segment_cost_it(seg, prod_vol=None):

            # get head of layers
            l = seg[0]
            tail = seg[1:]

            # Iterate over all possible schedules for this layer
            for sched in layer_space_recompute(
                l, prod_vol,
                tiling_func=args.tiling_func,
                loopreorder=args.loop_reorder
            ):
                cons_vol, cons_vol_folded, times, times_folded, \
                    wacc, oacc, dbuf, wbuf, obuf, macs = \
                    layer_cost(l, sched, prod_vol)

                # time to return
                tt = times_folded if prod_vol is not None else times

                if tail == []:
                    # if input, yield unfolded volumes and times
                    yield (
                        cons_vol.size, [times], [wacc], oacc,
                        [dbuf], wbuf, obuf,
                        [macs],
                        [sched]
                    )

                else:
                    # recurs, 'fuse' into input tile of current layer
                    for v, t, wa, oa, dbufs, max_wbuf, max_obuf, mcs, s in \
                            segment_cost_it(tail, cons_vol_folded):
                        yield (
                            v,
                            [times_folded] + t,
                            [wacc] + wa,
                            oa + oacc,  # can safely sum, only last layer contribs
                            [dbuf] + dbufs,
                            max(max_wbuf, wbuf),
                            # max is safe, only last layer contribs
                            max(max_obuf, obuf),
                            [macs] + mcs,
                            # N.B, schedules are returned producer to consumer ordered
                            s + [sched]
                        )

        costs = set()
        # reverse to build from consumer to producer
        rseg = list(reversed(seg))
        for idx, (vol, times, wacc, oacc, dbufs, wbuf, obuf, macs, sched_list) in \
                enumerate(segment_cost_it(rseg)):

            # reduce weight acesses
            scan_times = reduce(lambda l, e: l + [l[-1] * e], [[1]] + times)
            wacc = sum(map(op.mul, scan_times, wacc))

            # get multiply accumulates
            macs = sum(map(op.mul, scan_times, macs))

            # Ignore contributions of the output buffer
            if args.no_output_buf:
                obuf = 0
                oacc = 0

            ##########################################
            # First Compute for Unbuffered Input

            # accesses
            # data accesses is only input layer
            dacc = reduce(op.mul, times) * vol
            acc = dacc + wacc + oacc  # wacc is accumulated for all layers

            # compute buffer sizes
            dbuf = max2consec(dbufs)  # only 2 data buffers alive at a time
            buf = dbuf + wbuf + obuf

            # Create schedule
            ss = Segment([seg] + sched_list)
            ss.buffered_input = False
            ss.recompute = True

            # any double costs will simply be overwritten
            costs.add((acc, buf, macs, ss))

            ########################################
            # Next Compute for Buffered Input
            if args.buffered:
                # accesses
                input_layer = seg[0]  # get input layer of current segment
                vol = input_layer.cfg.cons_vol.size  # consumption volume of input layer
                dacc = vol          # data accesses is only input layer, and every element only once!
                acc = dacc + wacc + oacc  # wacc is accumulated for all layers

                # compute buffer sizes
                dbuf = max2consec(dbufs[:-1]) + vol  # +intermediate layers
                buf = dbuf + wbuf + obuf

                # Create schedule, and set to buffered
                ss = Segment([seg] + sched_list)
                ss.buffered_input = True
                ss.recompute = True

                # any double costs will simply be overwritten
                costs.add((acc, buf, macs, ss))

            ########################################
            # Check if exploration cap is reached
            if args is not None and args.exp_cap is not None and idx >= args.exp_cap - 1:
                print('Exiting from DSE, exceeded exploration cap')
                break

        # pareto filter section costs
        arr = np.array(list(costs))
        if args.pareto:
            arr = pareto(arr)
            print("  Explored %d schedules, of which %d are pareto" %
                  (idx + 1, len(arr)))
        else:
            print("  Explored %d schedules" % (idx + 1))
        segment2costs[tuple(seg)] = arr

    # return dictionary mapping sections -> (acc, mem, node_schedules)
    return segment2costs
