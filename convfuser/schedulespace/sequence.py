from .schedule import Partition
from .segment import segment_space
from ..utils import reduce_init
from itertools import product
from ..utils import pareto
import numpy as np


def partitions(l, max_len=None, min_len=None):
    """ Helper function to generate all partionings of a sequence"""
    if max_len is None:
        max_len = len(l)
    if min_len is None:
        min_len = 1
    for i in range(min_len, min(len(l), max_len + 1)):
        hd = l[0:i]
        tl = l[i:]
        for t in partitions(tl, max_len, min_len):
            yield [hd] + t
    if len(l) <= max_len and len(l) >= min_len:
        yield [l]


def add_points(a, b):
    """ Helper function that merges two points"""
    return tuple((
        a[0] + b[0],  # add accesses
        max(a[1], b[1]),  # max mem
        a[2] + b[2],  # add macs
        a[3] + b[3],  # concat segment schedules
    ))


def sequence_space(sequence, args=None):
    """ Get pareto schedules for sequence"""

    print('Exploring sequence:', [n.name for n in sequence])

    # Get maximum number of fused segments
    max_fused = None if args is None else args.max_fused
    min_fused = None if args is None else args.min_fused

    # Compute pareto schedules and costs per segment
    segment2costs = segment_space(sequence, args=args)

    # set of costs and partitionings
    seq_costs = set()

    # loop over all partitions
    for part in partitions(sequence, max_len=max_fused, min_len=min_fused):
        print(
            '  Combining segments into partition:',
            [[n.name for n in sec] for sec in part]
        )

        # Get segment costs of both recompute and non-recompute schedules
        seg_costs = [segment2costs[tuple(seg)] for seg in part]

        if not args.recompute:
            # if only non-recomputed, there is no relevant MAC dimension and
            # combining can be done smarter than a full product

            # get all unique buffer sizes
            bsizes = np.sort(np.unique([bsize for sc in seg_costs for bsize in sc[:, 1]]))
            # print(seg_costs[:, 1])

            # sort segment costs by buffer size
            sorted_seg_costs = [s[s[:, 1].argsort()] for s in seg_costs]

            # find minimum required B (max of all lowest Bs)
            Bmin = max([entries[0][1] for entries in sorted_seg_costs])

            # select only Bsizes equal to or above the minimum
            relevant_bsizes = bsizes[np.searchsorted(bsizes, Bmin, side='left'):]

            # loop over relevant buffer sizes
            for B in relevant_bsizes:
                # select segment schedules which just fit in B
                selected = np.asarray([
                    a[np.searchsorted(a[:, 1], B, side='right') - 1]
                    for a in sorted_seg_costs
                ])
                # reduce costs to single point
                point = (
                    np.sum(selected[:, 0]),  # sum of accesses
                    np.max(selected[:, 1]),  # max of buffer sizes
                    np.sum(selected[:, 2]),  # sum of macs
                    Partition(selected[:, 3])  # merge schedules into partition
                )
                seq_costs.add(point)

        else:
            # Full product required...
            def reduce_seg(a, b):
                lcost = set()
                for pa, pb in product(a, b):
                    lcost.add(add_points(pa, pb))
                if args.pareto:
                    return pareto(np.array(list(lcost)))
                return np.array(list(lcost))

            for c in reduce_init(
                reduce_seg, seg_costs,
                [(0, 0, 0, Partition())]
            ):
                seq_costs.add(tuple(c))

    # pareto filter the set of costs
    # return np.array(list(seq_costs))
    if args.pareto:
        return pareto(np.array(list(seq_costs)))
    return np.array(list(seq_costs))

