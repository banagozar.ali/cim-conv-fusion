import halide as hl
import numpy as np
from .halidefunc import HalideFunc


class Input_recomp(HalideFunc):
    """
    Simple input buffer
    """

    def __init__(self, input):

        # init halide func
        super().__init__()

        self.name = 'Input'

        # if input is tensor that can be converted to numpy, do so here
        if hasattr(input, 'numpy'):
            input = input.numpy()

        # Sanity check input
        assert (
            isinstance(input, np.ndarray) and
            "Input must be numpy array"
        )

        # Load halide variables into shorthands
        b = self.b
        m = self.m
        n = self.n
        o = self.o

        # Get unpadded input
        self.input_host = hl.Buffer(input, 'input_host')

        # Transfer input to device
        self.input_device = hl.Func('input_device')
        self.input_device[b, n, m, o] = self.input_host[b, n, m, o]

        # set main function
        self.f = self.input_device

        # set shape
        self.shape = input.shape

    def schedule(
        self, last=None, fuse_tgt=None, fuse_sched=None, verbose=False
    ):
        # shorthand
        f = self.f

        # if no schedule is passed, compute root
        if fuse_tgt is None and fuse_sched is None:
            if verbose:
                print("Computing input %s root" % (self.name))
            f.compute_root()
            f.store_root()
            return

        # default fuse target is self
        if fuse_tgt is None:
            fuse_tgt = self

        # get data store and compule levels
        comp_lvl = fuse_sched.cl.d
        store_lvl = fuse_sched.sl.d

        # Some nice reporting
        if verbose:
            print(
                'Fusing production of',
                self.name,
                'into',
                fuse_tgt.name,
                'compute at',
                comp_lvl,
                'store at',
                store_lvl,
            )

        f.store_at(fuse_tgt.f, getattr(self, store_lvl))
        f.compute_at(fuse_tgt.f, getattr(self, comp_lvl))

        # Provide some hints to halide on folding
        # (if it can fold, it will try to fold to this size)
        if 'm' in fuse_sched.fold_dims():
            f.fold_storage(self.m, fuse_tgt.Km, fold_forward=True)
        if 'n' in fuse_sched.fold_dims():
            f.fold_storage(self.n, fuse_tgt.Kn, fold_forward=True)
        if 'o' in fuse_sched.fold_dims():
            f.fold_storage(self.o, 1, fold_forward=True)


class Input(Input_recomp):
    def schedule(self, last=None, fuse_tgt=None, fuse_sched=None, verbose=False):
        self.f.store_root().compute_root()
