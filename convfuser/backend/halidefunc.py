import halide as hl
import numpy as np


class HalideFunc(object):
    """ Some common functions of halide functions """

    def __init__(self, layer=None):
        # Main halide function
        self.f = None

        # Create Loop Variables
        for v in list('mniob'):
            self.def_var(v)

        # Create Tiling Variables
        for l in list('oi'):
            for v in list('mno'):
                self.def_var('_'.join((v, l)))
            self.def_var('i_' + l, hl.RVar)

        # add all layer config attributes to self
        if layer is not None and hasattr(layer, 'cfg'):
            for k, v in layer.cfg.__dict__.items():
                setattr(self, k, v)

    def def_var(self, name, var_func=hl.Var):
        setattr(self, name, var_func(name))

    def __str__(self):
        if hasattr(self, 'name'):
            return self.name
        return str(self.__class__)

    def __call__(self, jit=None):
        # Get halide function
        assert(self.f is not None)
        f = self.f

        # get jit target
        if jit is None:
            jit = hl.get_jit_target_from_environment()

        # Realize
        output = f.realize(
            *self.shape,
            jit
        )

        return np.asarray(output)
