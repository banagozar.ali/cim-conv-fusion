from ..utils import iceil, ifloor, Namespace, ceildiv
from ..frontend.layerconfig import Volume


def times(l, s, prod_vol, folded=False):
    """ Calculate number of times input volume needs to be transferred
    to produce output volume 'vol'  """

    # Select less than function
    lt = s.lt_cl if folded else s.lt_sl

    # create complete volume
    _D = prod_vol + [l.Di]

    # N.B., range of s.T automatically excludes 'i' if needed for depth!=0
    times = 1
    for lvl, (D, T) in enumerate(zip(_D, s.T.values())):
        if (
            (lvl in [0, 1] and not lt(lvl)) or  # if in mn, check store or compute depending on folding lvl
            (lvl not in [0, 1] and not s.lt_cl(lvl))  # if o(i?) check compute lvl
        ):
            times *= D
        else:
            times *= ceildiv(D, T)

    return times


def cons_vol(l, s, folded=False):
    """ Compute dimensions of tiling in 'i'nput layer for given schedule """

    # Select less than function
    lt = s.lt_cl if folded else s.lt_sl

    # hold all dimensions
    vol = []

    for lvl in 'mn':
        if not lt(lvl):
            vol += [l.K[lvl]]
        else:
            vol += [(s.T[lvl] - 1) * l.S[lvl] + l.K[lvl]]

    # Di of this layer == Do of preceding layer
    Do = 1
    if 'i' not in s.dim_names:
        # not the root layer, always need complete input
        Do *= l.Di
    elif s.lt_cl('i'):  # TODO: always CL??
        # root layer, if i below the store level, need to transfer tiles of i
        Do *= s.T.i
    vol += [Do]

    return Volume(vol)


def wacc(l, s, prod_vol):
    _D = prod_vol + [l.Di]
    D = Namespace(**dict(zip('mnoi', _D)))

    # get transfer volume
    ret = l.K.m * l.K.n
    for lvl in 'o' if s.dims == 3 else 'oi':
        if s.lt_sl(lvl, data=False):
            ret *= s.T[lvl]

    # count times the volume is transferred
    for lvl in 'mno':
        if not s.lt_sl(lvl, data=False):
            ret *= D[lvl]
        else:
            ret *= ceildiv(D[lvl], s.T[lvl])

    # if fused, count full input
    if s.dims == 3 or (not s.lt_sl('i', data=False)):
        ret *= D['i']
    else:
        ret *= ceildiv(D['i'], s.T['i'])

    return ret


def wbuf(l, s):
    ret = l.K.m * l.K.n

    red = [lvl[0] for lvl in s.order[s._cl[0]:s._sl[0]]]
    for lvl in ('oi' if s.dims == 4 else 'o'):
        if s.lt_sl(lvl, data=False) and lvl not in red:
            ret *= s.T[lvl]

    if s.dims == 3:
        ret *= l.Di

    return ret


def dbuf(l, s):
    ret = 1
    red = [lvl[0] for lvl in s.order[s._cl[1]: s._sl[1]]]
    if s.dims == 4 and s.lt_sl('i') and 'i' not in red:
        ret *= s.T.i
    elif s.dims == 3:
        ret *= l.D.i
    for lvl in 'mn':
        if (lvl in red) or (not s.lt_sl(lvl)):
            ret *= l.K[lvl]
        else:
            ret *= (s.T[lvl] - 1) * l.S[lvl] + l.K[lvl]
    return ret


def obuf(l, s):
    # When fused, there is no output buffer (stored in next input)
    if s.dims == 3:
        return 0

    # only dimensions below i contribute
    ret = 1
    for d in 'omn':
        if s.lt(d, 'i'):
            ret *= s.T[d]
    return ret


def oacc(l, s):
    # when fused, results are never transferred partially, hence 0 accesses.
    if s.dims == 3:
        return 0
    return l.Do * l.Dm * l.Dn * (ceildiv(l.D.i, s.T.i) * 2 - 1)


def macs(l, s, prod_vol):
    """ muliply accumulates per production volume"""
    kernel_size = l.Km * l.Kn * l.Di
    return prod_vol.size * kernel_size
